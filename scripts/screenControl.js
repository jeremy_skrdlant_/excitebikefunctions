var isFirstManualRun = true;

function go(){
  document.getElementById("welcomeScreen").classList.add("hidden");
  document.getElementById("manPageInstructions").classList.remove("hidden");
  return "Awesome!\n You just called a function."
}

function man(){
  document.getElementById("manPage").classList.remove("hidden");
  if (isFirstManualRun){
    document.getElementById("manPageInstructions").classList.add("hidden");
    document.getElementById("gameArea").classList.remove("hidden");
    isFirstManualRun = false;
  }

  return "Here is your manual."
}

function quit(){
  document.getElementById("manPage").classList.add("hidden");
  return "The manual is always here when you need it."
}
