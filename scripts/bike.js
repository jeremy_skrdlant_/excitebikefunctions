// This lets us keep track of which tasks we have completed. 
// [laneUp(), laneDown(), doAWheelie(), addGas()]
var allChecks = [false, false, false, false];
// This is the lane of the motorcycle between 1 and 4
var currentLane = 1;
// This is a good time when true
var isInWheelie = false;
// This is how fast the obstacles move towards you which is 
// controlled by the addGas function.  It is all relative. 
// In this game the bike stays still and the world moves towards it.  
var obstacleSpeed = 0;
// This goes into the html and gets an element that has an id tag 
// of bike.  The element it finds is on line 48 of index.html
let bike = document.getElementById("bike");


//------------------------------------------------------------------//
//               Functions - The stuff that does stuff              //
//------------------------------------------------------------------//

/* 
 * Game is Go 
 *
 * This is like a safety check.  We do not want to throw obstacles at 
 * a rider until they know how to use the throttle, turn, and of course 
 * do a wheelie.
 */  
function gameIsAGo(){
  // We assume all is good
  var status = true;
  // We look through our list on line 3 and go through each element.
  allChecks.forEach((check, i) => {
    // ! means not.  So  (not false) is true.  Similar to how (not inDich) assumes 
    // you are still on the road and all is well. 
    if (!check){
      //We found a false which means the rider hasn't learned a skill.
      //we flag status as false. 
      status = false;
    }
  });
  //we return the status at the end of the loop.  
  //if we went through the loop and everything was true, our checklist was 
  //complete and we can start throwing obstacles at the rider. 
  //If there was one false anywhere in that list, we send back a false 
  //so that the game knows not to throw obstacles at the rider. 
  return status;
}


/*
 * Run Check
 *
 * I must have been low on coffe when I named this function.  We will refactor it 
 * together in a bit. 
 * This does not run a check on an element.  It sets it from not done to done. 
 * it takes the index reffering to which spot in the array and an elementID it can 
 * find on the html page. It does let you know if it has already been checked.
 */ 
function runCheck(index, elementID){
  // we check to see if the item is a new item.  We are changing from 
  // false to true. 
  if (!allChecks[index]){
    // We certify this rider has done the task. 
    allChecks[index] = true;
    // We get the element from the index.html (line 36 - 41). 
    let check = document.getElementById(elementID)
    // We change the style by adding a class to it. 
    // This changes the background color. 
    check.classList.add("complete");
    // We set the text inside the circle to be a checkmark
    check.innerHTML = "✓"
    // We play a beeping sound to boost confidence in your riding ability.
    document.getElementById("lowBeep").play();
    // We return true to let the program know it was a new learned skill
    return true;
  }
  // This means it was already checked.  The rider is just revving the throttle without 
  // learning to change lanes.  Typical new rider. 
  return false;
}

/*
 * Start Game
 * 
 * We have been onboarded.  now time to avoid those sage brush trees. 
 */
function startGame(){
  // This waits a specific amount of time before doing something.  
  // the 1000 represents milliseconds so 1 second. This will give 
  // the user 1 second of satisfaction seeing all their passing checkmarks.
  setTimeout(()=>{
    //get and play a high beep to let them know its time to race.
    document.getElementById("highBeep").play();
    //hide our checkmarks area so we can see the entire track.
    document.getElementById("functionTrial").classList.add("hidden")
  },1000)

  //we start by adding an obstacle to the first lane. 
  let lane1 = document.getElementById("lane1");
  createObstacle(lane1)

  //setTimeout runs once, setInterval runs forever. 
  //Every 3 seconds we add a new obstacle, every 20 milliseconds we 
  //update the objects on the screen. 
  let obstacleTimer = setInterval(addRandomObstacle, 3000);
  let moveTimer = setInterval(updateGame, 20)
}

 /*
  * Add Gas
  * 
  * I am pretty sure all dirt bike riders call twisting the throttle adding gas. 
  * Yup, we are just adding speed. 
  */
function addGas(){
  // Add our checkmark if it doesn't already exist. 
  if (runCheck(0, "gasCheck")){
    if(gameIsAGo()){
      //start the game if all is well. (line 87)
      startGame()
    }
    //This prints out to the console. 
    console.log("New speed of " + obstacleSpeed);
    return "You have found the throttle!"
  }
  // we are going one pixel per 20 milliseconds faster.  
  // Yee Haw!!
  obstacleSpeed += 1;
  return "More Speed"
}

/**
 *  Lane Up 
 * 
 *  This moves our motorcycle up to the next lane up. 
 */
function laneUp(){
  //same check as add gas
  if (runCheck(1, "laneUpCheck")){
    if(gameIsAGo()){
      startGame()
    }
    return "You moved up a lane."
  }
  //We check to make sure we do not go out of bounds. 
  //We do not want to go in lane 5 of a 4 lane track. 
  if (currentLane < 4){
    currentLane++;
    //We call a function that takes a number and puts us 
    //in that lane. 
    changeLane(currentLane);
  }
  return "Changing Lanes";
}

/**
 *  Lane Down 
 *  
 * This is the same as lane up except we check to make 
 * sure we do not go below zero. 
 */
function laneDown(){
  var phrase = "Changing Lanes";
  if (runCheck(2, "laneDownCheck")){
    if(gameIsAGo()){
      startGame()
    }
    phrase = "You moved down a lane!";
  }
  if (currentLane > 1){
    currentLane--;
    changeLane(currentLane);
  }
  return phrase;
}

/**
 * Change Lane
 * 
 * This does the actual lane changing. It is actually really 
 * cool how it works.  If you append an existing item to a new parent, 
 * it will automatically take them out of the old parent. 
 * 
 * Imagine moving your grocery item to a new sack without having to 
 * worry about removing it from the original sack. 
 */
function changeLane(laneNumber){
  //we have a number 1 through 4.  If the case matches our 
  //number, we change lanes.  
  switch (laneNumber){
    case 1:
    document.getElementById("lane1").appendChild(bike);
    break;
    case 2:
    document.getElementById("lane2").appendChild(bike);
    break;
    case 3:
    document.getElementById("lane3").appendChild(bike);
    break;
    case 4:
    document.getElementById("lane4").appendChild(bike);
    break;
  }
  return 
  //Note - there are always multiple ways of doing things.  
  //one common thing developers often do is play code golf.  The 
  //less number of lines to get something to work wins. 
  // This line of code does the same thing. It is gray because I did 
  // a return above so it will never run. Even though it is a magnificent 
  // bit of code.  It will neve run.  
  document.getElementById(`lane${laneNumber}`).appendChild(bike)
}

/**
 *  Do A Wheelie
 * 
 * This is the most important function of all time. 
 */
function doAWheelie(){
  if (runCheck(3, "wheelieCheck")){
    if(gameIsAGo()){
      startGame()
    }
  }
  bike.classList.add("wheelie");
  isInWheelie = true;
  setTimeout(()=>{
    isInWheelie = false;
    bike.classList.remove("wheelie");
  }, 2000)
  return "Wheelie Time!!";
}

/**
 * Create Obstacle 
 * 
 * This is that pesky function that is littering the track with 
 * sage brushes.  It can be fixed though.  If we change the image
 * from a bonsaie tree to a ramp, It will make the track so 
 * much better. 
 * 
 * Not to self, I need to change all references from sage brush trees 
 * to bonsai trees. ProcrastinationPoints += 1
 */
function createObstacle(lane){
  //We create a brand new image tag like all the other tags in the 
  //index.html
  let newObstacle = document.createElement("img");
  //We set the image.  Open up the images folder and find the ramp
  //Modify this so it is a ramp and then play the game through again.
  //See how much better that is. 
  newObstacle.src = "images/Bonsaie.png";
  //We give it that obstacle style
  newObstacle.classList.add("obstacle");
  //We place it at the far right of the window. 
  newObstacle.style.left = window.innerWidth + 'px';
  //We append this obstacle to the lane it will stay in.
  lane.appendChild(newObstacle);
}

/**
 * Add Random Obstacle
 * 
 * This funcion add randomness to the game.  Without it, 
 * all our obstacles would be on lane 1.  Pretty easy race to 
 * win there. 
 */
function addRandomObstacle(){
  //We are nice, we will not pile on obstacles while you are sitting on your 
  //bike at 0 speed throttle. 
  if(obstacleSpeed == 0){
    //Note, nothing in a function after a return runs
    return;
  }

  //We have speed, we generate a number between 1 and 4. 
  let laneNumber = Math.floor(Math.random() * 4) + 1; 
  //we change it from 1 to lane1 so that we can grab the element.
  let lane = `lane${laneNumber}`
  let laneElement = document.getElementById(lane);
  //We call our function above to add a happy little tree to the race 
  //track.  
  createObstacle(laneElement);
}


/*
 * Update Game 
 * 
 * This is similar to a game loop.  It is called lower down in the code 
 * every 20 milliseconds.  It is in control of moving all the obstacles 
 * and forcing your bike to crash if you are not careful.
*/
function updateGame(){
  // Instead of getting an element by id, this is getting all the 
  // obstacles on the page.  So sage brushes to begin with and ramps 
  // later on. 
  let obstacles = document.querySelectorAll(".obstacle");
  // This does nothing for the game, it helps the developer 
  // troubleshoot things by seeing the value of a variable change. 
  console.log("Speed of " + obstacleSpeed);
  // We go through each obstacle that we found on the list.  We are going 
  // to identify them one at a time and see if they crash our bike. 
  obstacles.forEach((obstacle, i) => {
    // We are getting how far the obstacle is from the left side 
    // of the screen. parseInt converts it from '200px' to 200 
    let leftAmount = parseInt(obstacle.style.left);
    // if our speed is 5, we change our left amount from 200 to 195.
    // slowly that obstacle is coming towards us. 
    leftAmount -= obstacleSpeed;
    // Our bike takes up the pixels of 120 to 250.  If the obstacle is between 
    // there, then we might be in trouble as a rider. 
    if (leftAmount < 250 && leftAmount > 120){
      //Each obstacle is polite and stays in it's lane.  
      //We get the id of the parent which is the lane.  
      //Think of it like adding a grocery item to a bag.  It's parent is that 
      // bag so you move the bag, the grocery items moves as well. 
      let obstacleLane = obstacle.parentNode.id;
      // our current lane is a number and the lanes have the word lane before it. 
      // this variable temporarily gets our lane as lane1 or lane2 or ...
      let bikeLane = `lane${currentLane}`
      //Uh oh, if the obstical is in our lane and we are in our lane, 
      // then possibly crash?? It all depends on the bikeHitRamp function. 
      if(obstacleLane == bikeLane){
        //Well our bike it the obstacle so we are assuming its a ramp I guess. 
        //We will call that function and see what happens. 
        bikeHitRamp()
        //This is duck tape if I ever seen it.  I didn't want a double hit so I 
        //just got rid of it instead of marking it notEnabled.  
        //Dang lazy programmers :)
        obstacle.parentNode.removeChild(obstacle);
      }
    }
    //This is cleanup, if the obsacle is about to go off screen, 
    //then we will remove it.  If not, we will just move it to the left 
    //using our new left value. 
    if(leftAmount < 20){
      obstacle.parentNode.removeChild(obstacle);
    }else{
      obstacle.style.left = leftAmount + "px";

    }
  })
}

/**
 * Bike Hit Ramp 
 * 
 * In the original Excite Bike game, hitting a ramp in wheelie 
 * mode was epic.  Hitting it when not in wheelie mode resulted 
 * in a crash and the shame of you having to pick up your 
 * bike in front of the entire crowd. 
 */
function bikeHitRamp(){
  //Are we in wheelie mode. 
  if (isInWheelie){
    //We are going over the ramp!!!
    bike.classList.remove("wheelie")
    bike.classList.add("jumping");
    //We get 2 seconds of air time before we remove the jumping class.
    setTimeout(()=>{
      bike.classList.remove("jumping");
    }, 2000);
    return "Awesome Jump";
  }else{
    //Well, we were not in wheelie mode.  :(
    bike.classList.add("crash");
    document.getElementById("crash").play();
    setTimeout(()=>{
      bike.classList.remove("crash");
      currentLane = 1;
      changeLane(currentLane);
    }, 1000);
    return "Bummer, you crashed"
  }
}

//Thank you for checking out this code. 